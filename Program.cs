﻿using System;

namespace exercise_17
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            var bestPhone="";            
            start:
            System.Console.WriteLine("What is the better device, iphone or android?\n");
            bestPhone = Console.ReadLine();
            if (bestPhone == "iphone")
            {
                System.Console.WriteLine("\nIm sorry that is incorrect please try again!\n");
                goto start;
            }       
            else if (bestPhone=="android") 
            {
                System.Console.WriteLine("\nCORRECT!");                 
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine(); 
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
            }
            else 
            {
                System.Console.WriteLine("\nPlease choose a valid answer, iphone or android?\n");
                goto start;
            }
            
        }
    }
}
